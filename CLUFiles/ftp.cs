﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Text;

namespace CLUFiles
{
    public class ftp
    {
        public string HttpFileGetReq(Uri uri, int reqTimeout, Encoding encoding)
        {

            var req = (HttpWebRequest)WebRequest.Create(uri);

            req.Timeout = reqTimeout;

            var res = (HttpWebResponse)req.GetResponse();

            using (var receiveStream = res.GetResponseStream())
            {
                using (var readStream = new StreamReader(receiveStream, encoding))
                {
                    return readStream.ReadToEnd();
                }
            }
        }

        public static System.IO.Stream GetFileAsStream(string ftpUrl, string username, string password, bool usePassive)
        {
            WebClient request = new WebClient();
            Uri uri = new Uri(ftpUrl);


            var req = (HttpWebRequest)WebRequest.Create(ftpUrl);
            req.Credentials = new NetworkCredential(username, password);

            var res = (HttpWebResponse)req.GetResponse();


            System.IO.Stream fileResponseStream;

            System.Net.FtpWebResponse fileResponse = (System.Net.FtpWebResponse)req.GetResponse();

            fileResponseStream = fileResponse.GetResponseStream();

            return fileResponseStream;
        }

        public static long GetFileLength(string ftpUrl, string username, string password, bool usePassive)
        {
            System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)System.Net.WebRequest.Create(ftpUrl);

            request.KeepAlive = false;
            request.UsePassive = usePassive;


            request.Credentials = new System.Net.NetworkCredential(username, password);
            request.Method = System.Net.WebRequestMethods.Ftp.GetFileSize;

            System.Net.FtpWebResponse lengthResponse = (System.Net.FtpWebResponse)request.GetResponse();
            long length = lengthResponse.ContentLength;
            lengthResponse.Close();
            return length;

        }

        public void DisplayFileFromServer(string serverUri, Encoding encoding, string savefileloc)
        {

            // Get the object used to communicate with the server.
            WebClient request = new WebClient();
            Uri uri = new Uri(serverUri);


            var req = (HttpWebRequest)WebRequest.Create(serverUri);
            req.Credentials = new NetworkCredential("dcuris_ec", "cNh0u4dk");

            var res = (HttpWebResponse)req.GetResponse();
            Stream output = null;
            using (output = File.OpenWrite(savefileloc))
            using (Stream input = res.GetResponseStream())
            {
                input.CopyTo(output);
            }



        }
        public string ProcessStream(Stream inputStream)
        {
            //Sets the Position of the Stream to 0 (so the complete stream is read)
            //inputStream.Position = 0;

            using (StreamReader reader = new StreamReader(inputStream))
            {
                //This returns the stream as a string
                return reader.ReadToEnd();
            }
        }

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }
        public void SaveStreamToFile(string fileFullPath, Stream stream)
        {
            if (stream.Length == 0) return;

            // Create a FileStream object to write a stream to a file
            using (FileStream fileStream = System.IO.File.Create(fileFullPath, (int)stream.Length))
            {
                // Fill the bytes[] array with the stream data
                byte[] bytesInStream = new byte[stream.Length];
                stream.Read(bytesInStream, 0, (int)bytesInStream.Length);

                // Use FileStream object to write to the specified file
                fileStream.Write(bytesInStream, 0, bytesInStream.Length);
            }
        }

    }
}