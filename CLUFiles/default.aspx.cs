﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CharterData;
using System.Xml.Linq;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Net;
using System.Text;
using CLUFiles;
using System.Text.RegularExpressions;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Web.Configuration;

namespace CLUFiles
{

    public partial class _default : System.Web.UI.Page
    {
        public string CurrentDate
        {
            get
            {
                // Returns the Current DateTime string.
                return System.DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        public string FTPServer
        {
            get
            {
                // Returns the FTP Server Name string.
                return WebConfigurationManager.AppSettings["FTPServer"];
            }
        }
        public string UserName
        {
            get
            {
                // Returns the UserName string.
                return WebConfigurationManager.AppSettings["UserName"];
            }
        }
        public string Password
        {
            get
            {
                // Returns the Password string.
                return WebConfigurationManager.AppSettings["Password"];
            }
        }
        public string JsonCallback { get; set; }

        public string Serialize(object value)
        {
            Type type = value.GetType();

            Newtonsoft.Json.JsonSerializer json = new Newtonsoft.Json.JsonSerializer();

            json.NullValueHandling = NullValueHandling.Ignore;

            json.ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Replace;
            json.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
            json.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            if (type == typeof(DataTable))
                json.Converters.Add(new DataTableConverter());
            else if (type == typeof(DataSet))
                json.Converters.Add(new DataSetConverter());

            StringWriter sw = new StringWriter();
            Newtonsoft.Json.JsonTextWriter writer = new JsonTextWriter(sw);

            writer.Formatting = Formatting.Indented;

            writer.QuoteChar = '"';
            json.Serialize(writer, value);

            string output = sw.ToString();
            writer.Close();
            sw.Close();

            return output;
        }

        public object Deserialize(string jsonText, Type valueType)
        {
            Newtonsoft.Json.JsonSerializer json = new Newtonsoft.Json.JsonSerializer();

            json.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            json.ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Replace;
            json.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
            json.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            StringReader sr = new StringReader(jsonText);
            Newtonsoft.Json.JsonTextReader reader = new JsonTextReader(sr);
            object result = json.Deserialize(reader, valueType);
            reader.Close();

            return result;
        }
        protected void CreateDirectory()
        {
            

            string DirectoryName = Server.MapPath(System.DateTime.Now.ToString("yyyy-MM-dd"));
            Directory.CreateDirectory(DirectoryName);

        }

        protected void GetListofFiles(string serverUri)
        {
            // Get the object used to communicate with the server.
            WebClient request = new WebClient();
            Uri uri = new Uri(serverUri);


            var req = (HttpWebRequest)WebRequest.Create(serverUri);

            req.Credentials = new NetworkCredential(UserName, Password);
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            var res = (HttpWebResponse)req.GetResponse();

            if (res.StatusCode == HttpStatusCode.OK)
            {
                Stream rs = res.GetResponseStream();
                StreamReader sr = new StreamReader(rs);
                StringBuilder sb = new StringBuilder();


                char[] read = new Char[256];
                int count = sr.Read(read, 0, 256);


                while (count > 0)
                {
                    sb.Append(read, 0, count);
                    count = sr.Read(read, 0, 256);
                }
                sr.Close();
                res.Close();


                // Extract <a href=... </a> from html
                string expression = @"<(?<Tag_Name>(a)|img)\b[^>]*?\b(?<URL_Type>(?(1)href|src))\s*=\s*(?:""(?<URL>(?:\\""|[^""])*)""|'(?<URL>(?:\\'|[^'])*)')";
                Regex regEx = new Regex(expression, RegexOptions.Multiline | RegexOptions.IgnoreCase);
                MatchCollection matches = regEx.Matches(sb.ToString());


                foreach (Match match in matches)
                {
                    // extract href attribute value
                    string href = Regex.Match(match.Value, "([\"\"'](?<url>.*?)[\"\"'])", RegexOptions.Multiline | RegexOptions.IgnoreCase).Value;
                    href = href.Replace("\"", "");


                    // foldernames start and end with / but files only start with /
                    if (href.StartsWith("/") && !href.EndsWith("/"))
                    {
                        string fileToGet = FTPServer + href;
                        string toBeSaved = Server.MapPath(href);
                        if (File.Exists(toBeSaved) == false)
                        {
                            WebClient wc = new WebClient();
                            WebClient client = new WebClient();
                            client.Credentials = new NetworkCredential("dcuris_ec", "cNh0u4dk");
                            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                            System.Uri uri2 = new Uri(fileToGet);
                            client.DownloadFile(uri2, toBeSaved);
                        }
                    }
                }

            }

        }

        protected void GetFiles(string CurrentDate)
        {
            string[] fileEntries = Directory.GetFiles(Server.MapPath(CurrentDate));
            System.Collections.ArrayList ar = new System.Collections.ArrayList();
            DataSet clone = new DataSet();
            foreach (string fileName in fileEntries)
            {
                DataSet ds = PutXMLintoDS(fileName);
                ar.Add(ds);
                CreateFiles(ds, fileName);
                clone = ds.Clone();
            }
            CreateMappingFiles(ar, clone);
        }

        protected void ProcessFiles()
        {
            CreateDirectory();
            GetListofFiles(FTPServer + CurrentDate + "/");
            GetFiles(CurrentDate);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ProcessFiles();
            
            //Note to me - now that they have regions all broken out, do not need to "create files" Simply need to serialize ds into Json and write to a file
            Response.Write("Done");
        }

        public void CreateFiles(DataSet ds,string filename)
        {
            using (StreamWriter sw = File.CreateText(filename + ".json"))
            {
                sw.Write(string.Format("{0}({1});", "parseChannel", Serialize(ds)));
            }
        }

        public void CreateMappingFiles(System.Collections.ArrayList ar, DataSet ds)
        {
            ds.EnforceConstraints = false;
            foreach (DataSet newDS in ar)
            {
                DataRow[] rowsToCopy;
                //try
                //{
                //    rowsToCopy = newDS.Tables["Regions"].Select();

                //    // Grab all region data

                //    foreach (DataRow temp in rowsToCopy)
                //    {
                //        ds.Tables["Regions"].ImportRow(temp);
                //    }
                //}
                //catch 
                //{
                //    //Do Nothing
                //}
               
                //rowsToCopy = newDS.Tables["CLURegion"].Select();

                //// Grab all region data


                //foreach (DataRow temp in rowsToCopy)
                //{
                //    ds.Tables["CluRegion"].ImportRow(temp);
                //}
               

       
                try
                {

                    rowsToCopy = newDS.Tables["CLUSysPrins"].Select();

                    // Grab all region data

                    foreach (DataRow temp in rowsToCopy)
                    {
                        ds.Tables["CLUSysPrins"].ImportRow(temp);
                    }
                }
                catch
                {
                    //Do Nothing
                }
                try
                {
                    rowsToCopy = newDS.Tables["CLUSysPrin"].Select();

                    // Grab all region data

                    foreach (DataRow temp in rowsToCopy)
                    {
                        ds.Tables["CLUSysPrin"].ImportRow(temp);
                    }
                }
                catch
                {
                    //Do Nothing
                }

            
            }
            
            //ds.Relations.Clear();
            //ds.Tables["ServiceOffering"].Constraints.Clear();
            //ds.Tables["CLUServiceOffering"].Constraints.Clear();
            //ds.Tables["CLUServiceOfferings"].Constraints.Clear();
            
            //ds.Tables["MasterSection"].Constraints.Clear();
            ////ds.Tables["CLUChannel"].Constraints.Clear();
            ////ds.Tables["CLUChannels"].Constraints.Clear();
            ////ds.Tables["MasterChannel"].Constraints.Clear();
            ////ds.Tables["MasterChannels"].ChildRelations.Clear();
            ////ds.Tables["MasterChannels"].ParentRelations.Clear();
            ////ds.Tables["MasterChannel"].ChildRelations.Clear();
            ////ds.Tables["MasterChannel"].ParentRelations.Clear();
            ////ds.Tables["CLUChannels"].ChildRelations.Clear();
            ////ds.Tables["CLUChannels"].ParentRelations.Clear();
            ////ds.Tables["CLUChannel"].ChildRelations.Clear();
            ////ds.Tables["CLUChannel"].ParentRelations.Clear();
            ////ds.Tables["MasterChannels"].Constraints.Clear();

            
            
            ////ds.Tables["CLUSection"].Constraints.Clear();
            ////ds.Tables["CLUSections"].Constraints.Clear(); 
            //ds.Tables["CLUSysPrin"].Constraints.Clear();
            //ds.Tables["CLUSysPrins"].Constraints.Clear();
            //ds.Tables["CLURegion"].Constraints.Clear();
            
            //ds.Tables.Remove("Regions");
            ////ds.Tables.Remove("CLUSections");
            ////ds.Tables.Remove("CLUSection");
            ////ds.Tables.Remove("CLUChannels");
            ////ds.Tables.Remove("CLUChannel");
            ////ds.Tables.Remove("MasterChannels");
            ////ds.Tables.Remove("MasterChannel");
            //ds.Tables.Remove("MasterSection");
            //ds.Tables.Remove("CLUServiceOfferings");
            //ds.Tables.Remove("CLUServiceOffering");
            //ds.Tables.Remove("ServiceOffering");
            //ds.Tables.Remove("CLUSysPrins");
            using (StreamWriter sw = File.CreateText(Server.MapPath(CurrentDate + @"\" + "region-mapping-json.json")))
            {
                sw.Write(string.Format("{0}({1});", "parseData", Serialize(ds)));
            }
        }

        public DataSet ReturnNeededData(DataSet ds, string RegionsID)
        {
            DataSet dataSet = ds;

            DataSet dsTarget = new DataSet();
            dsTarget = dataSet.Clone();

            try
            {


                DataRow[] rowsToCopy = dataSet.Tables["Regions"].Select();

                // Grab all region data

                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["Regions"].ImportRow(temp);
                }

                // Grab all data in CLURegions

                rowsToCopy = dataSet.Tables["CLURegion"].Select("CLURegion_id in (" + RegionsID + ")");
                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["CLURegion"].ImportRow(temp);
                }



                rowsToCopy = dataSet.Tables["CLUSections"].Select("CLURegion_id = " + RegionsID);
                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["CLUSections"].ImportRow(temp);
                }


                string regions = "";
                for (int i = 0; i < dsTarget.Tables["CLUSections"].Rows.Count; i++)
                {
                    regions += dsTarget.Tables["CLUSections"].Rows[i]["CLUSections_id"].ToString();
                    if (i < dsTarget.Tables["CLUSections"].Rows.Count - 1)
                    {
                        regions += ",";
                    }

                }
                rowsToCopy = dataSet.Tables["CLUSection"].Select("CLUSections_ID in (" + regions + ")");
                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["CLUSection"].ImportRow(temp);
                }
                try
                {
                    regions = "";
                    for (int i = 0; i < dsTarget.Tables["CLUSection"].Rows.Count; i++)
                    {
                        regions += dsTarget.Tables["CluSection"].Rows[i]["CLUSection_Id"].ToString();
                        if (i < dsTarget.Tables["CLUSection"].Rows.Count - 1)
                        {
                            regions += ",";
                        }

                    }
                    rowsToCopy = dataSet.Tables["CLUChannels"].Select("CLUSection_ID in (" + regions + ")");
                    foreach (DataRow temp in rowsToCopy)
                    {
                        dsTarget.Tables["CLUChannels"].ImportRow(temp);
                    }
                }
                catch (Exception ex)
                {

                }
                regions = "";
                for (int i = 0; i < dsTarget.Tables["CLUSection"].Rows.Count; i++)
                {
                    regions += dsTarget.Tables["CLUChannels"].Rows[i]["CLUChannels_Id"].ToString();
                    if (i < dsTarget.Tables["CLUChannels"].Rows.Count - 1)
                    {
                        regions += ",";
                    }

                }
                rowsToCopy = dataSet.Tables["CLUChannel"].Select("CLUChannels_Id in (" + regions + ")");
                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["CLUChannel"].ImportRow(temp);
                }

                regions = "";
                for (int i = 0; i < dsTarget.Tables["CLUChannel"].Rows.Count; i++)
                {
                    regions += dsTarget.Tables["CLUChannel"].Rows[i]["CLUChannel_Id"].ToString();
                    if (i < dsTarget.Tables["CLUChannel"].Rows.Count - 1)
                    {
                        regions += ",";
                    }

                }
                rowsToCopy = dataSet.Tables["MasterChannels"].Select("CLUChannel_Id in (" + regions + ")");
                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["MasterChannels"].ImportRow(temp);
                }

                regions = "";
                for (int i = 0; i < dsTarget.Tables["MasterChannels"].Rows.Count; i++)
                {
                    regions += dsTarget.Tables["MasterChannels"].Rows[i]["MasterChannels_Id"].ToString();
                    if (i < dsTarget.Tables["MasterChannels"].Rows.Count - 1)
                    {
                        regions += ",";
                    }

                }
                rowsToCopy = dataSet.Tables["MasterChannel"].Select("MasterChannels_Id in (" + regions + ")");
                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["MasterChannel"].ImportRow(temp);
                }


                regions = "";
                for (int i = 0; i < dsTarget.Tables["CLUSection"].Rows.Count; i++)
                {
                    regions += dsTarget.Tables["CLUSection"].Rows[i]["CLUSection_ID"].ToString();
                    if (i < dsTarget.Tables["CLUSection"].Rows.Count - 1)
                    {
                        regions += ",";
                    }

                }
                rowsToCopy = dataSet.Tables["MasterSection"].Select("CLUSection_ID in (" + regions + ")");
                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["MasterSection"].ImportRow(temp);
                }


                regions = "";
                for (int i = 0; i < dsTarget.Tables["CLURegion"].Rows.Count; i++)
                {
                    regions += dsTarget.Tables["CLURegion"].Rows[i]["CLURegion_ID"].ToString();
                    if (i < dsTarget.Tables["CLURegion"].Rows.Count - 1)
                    {
                        regions += ",";
                    }

                }
                rowsToCopy = dataSet.Tables["CLUServiceOfferings"].Select("CLURegion_ID in (" + regions + ")");
                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["CLUServiceOfferings"].ImportRow(temp);
                }


                regions = "";
                for (int i = 0; i < dsTarget.Tables["CLUServiceOfferings"].Rows.Count; i++)
                {
                    regions += dsTarget.Tables["CLUServiceOfferings"].Rows[i]["CLUServiceOfferings_ID"].ToString();
                    if (i < dsTarget.Tables["CLUServiceOfferings"].Rows.Count - 1)
                    {
                        regions += ",";
                    }

                }
                rowsToCopy = dataSet.Tables["CLUServiceOffering"].Select("CLUServiceOfferings_ID in (" + regions + ")");
                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["CLUServiceOffering"].ImportRow(temp);
                }


                regions = "";
                try
                {
                    for (int i = 0; i < dsTarget.Tables["CLUServiceOffering"].Rows.Count; i++)
                    {
                        regions += dsTarget.Tables["CLUServiceOffering"].Rows[i]["CLUServiceOffering_Id"].ToString();
                        if (i < dsTarget.Tables["CLUServiceOffering"].Rows.Count - 1)
                        {
                            regions += ",";
                        }

                    }
                    rowsToCopy = dataSet.Tables["ServiceOffering"].Select("CLUServiceOffering_Id in (" + regions + ")");
                    foreach (DataRow temp in rowsToCopy)
                    {
                        dsTarget.Tables["ServiceOffering"].ImportRow(temp);
                    }
                }
                catch
                {

                }

                regions = "";
                for (int i = 0; i < dsTarget.Tables["CLURegion"].Rows.Count; i++)
                {
                    regions += dsTarget.Tables["CLURegion"].Rows[i]["CLURegion_ID"].ToString();
                    if (i < dsTarget.Tables["CLURegion"].Rows.Count - 1)
                    {
                        regions += ",";
                    }

                }
                rowsToCopy = dataSet.Tables["CLUSysPrins"].Select("CLURegion_ID in (" + regions + ")");
                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["CLUSysPrins"].ImportRow(temp);
                }


                regions = "";
                for (int i = 0; i < dsTarget.Tables["CLUSysPrins"].Rows.Count; i++)
                {
                    regions += dsTarget.Tables["CLUSysPrins"].Rows[i]["CLUSysPrins_Id"].ToString();
                    if (i < dsTarget.Tables["CLUSysPrins"].Rows.Count - 1)
                    {
                        regions += ",";
                    }

                }
                rowsToCopy = dataSet.Tables["CLUSysPrin"].Select("CLUSysPrins_Id in (" + regions + ")");
                foreach (DataRow temp in rowsToCopy)
                {
                    dsTarget.Tables["CLUSysPrin"].ImportRow(temp);
                }
            }
            catch (Exception ex)
            {

            }

            return dsTarget;

        }
        public DataSet PutXMLintoDS(string xml)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(xml);
            return ds;
        }
        static CharterServiceXMLTablesData GetAttachedFileNames(string filename)
        {
            //instantiate a new CharterService Table
            CharterData.CharterServiceXMLTablesData td = new CharterServiceXMLTablesData();

            //Start Parsing each individual Region

            XDocument document = XDocument.Load(filename);
            foreach (XElement des in document.Descendants())
            {
                CharterServiceXMLTablesDataCLURegion region = new CharterServiceXMLTablesDataCLURegion();

                if (des.Name.ToString().Contains("CLURegion"))
                {
                    Console.WriteLine(des.Name);

                    CharterServiceXMLTablesDataCLURegionCLUSection section = new CharterServiceXMLTablesDataCLURegionCLUSection();
                    foreach (XElement des2 in des.Elements())
                    {
                        switch (des2.Name.LocalName)
                        {
                            case "Agent":
                                region.Agent = des2.Value;
                                break;
                            case "CLUSections":
                                foreach (XElement des3 in des2.Elements())
                                {
                                    switch (des3.Name.LocalName)
                                    {
                                        case "CLUSection":
                                            foreach (XElement des4 in des3.Elements())
                                            {
                                                switch (des4.Name.LocalName)
                                                {
                                                    case "CLUChannels":
                                                        foreach (XElement des5 in des4.Elements())
                                                        {
                                                            switch (des5.Name.LocalName)
                                                            {
                                                                case "CLUChannel":
                                                                    CharterServiceXMLTablesDataCLURegionCLUSectionCLUChannel channel = new CharterServiceXMLTablesDataCLURegionCLUSectionCLUChannel();

                                                                    foreach (XElement des6 in des5.Elements())
                                                                    {
                                                                        switch (des6.Name.LocalName)
                                                                        {
                                                                            case "ChannelLineupID":
                                                                                channel.ChannelLineupID = Convert.ToInt64(des6.Value);
                                                                                break;
                                                                            case "ChannelNumber":
                                                                                channel.ChannelNumber = des6.Value;
                                                                                break;
                                                                            case "MasterChannelDescription":
                                                                                channel.MasterChannelDescription = des6.Value;
                                                                                break;
                                                                            case "MasterChannelID":
                                                                                channel.MasterChannelID = Convert.ToInt64(des6.Value);
                                                                                break;
                                                                            case "MasterChannels":
                                                                                CharterServiceXMLTablesDataCLURegionCLUSectionCLUChannelMasterChannel mc = new CharterServiceXMLTablesDataCLURegionCLUSectionCLUChannelMasterChannel();

                                                                                foreach (XElement des7 in des6.Elements())
                                                                                {
                                                                                    switch (des7.Name.LocalName)
                                                                                    {
                                                                                        case "MasterChannel":
                                                                                            foreach (XElement des8 in des7.Elements())
                                                                                            {
                                                                                                switch (des8.Name.LocalName)
                                                                                                {
                                                                                                    case "ChannelId":
                                                                                                        mc.ChannelId = Convert.ToInt64(des8.Value);
                                                                                                        break;
                                                                                                    case "ChannelName":
                                                                                                        mc.ChannelName = des8.Value;
                                                                                                        break;
                                                                                                    case "Description":
                                                                                                        mc.Description = des8.Value;
                                                                                                        break;
                                                                                                }
                                                                                            }
                                                                                            break;
                                                                                    }

                                                                                }
                                                                                channel.MasterChannels.Add(mc);
                                                                                break;

                                                                        }
                                                                    }
                                                                    section.CLUChannels.Add(channel);
                                                                    break;
                                                            }
                                                        }
                                                        break;
                                                }
                                            }
                                            break;
                                    }
                                }
                                break;

                            case "ChannelLineUpName":
                                region.ChannelLineUpName = des2.Value;
                                break;
                            case "ChannelRegionID":
                                region.ChannelRegionID = Convert.ToInt64(des2.Value);
                                break;
                            case "charterdate":
                                region.charterDate = des2.Value;
                                break;

                        }
                        region.CLUSections.Add(section);
                    }
                    td.Regions.Add(region);
                }

            }
            return td;
        }



        public CharterServiceXMLTablesData ParseXML()
        {
            CharterServiceXMLTablesData regions = GetAttachedFileNames(Server.MapPath("TestData.xml"));
            return regions;
        }
    }
}